# Võ Thiện Quốc
# 2180601266
# 21DTHD4
# Khoa: Công Nghệ Thông Tin
##

|Title:|Staff logins Account|
|------|------------|
| value Statement:| As a dedicated restaurant server, I aspire to log into the restaurant management application, enabling me to swiftly and efficiently place orders for our esteemed customers.|
| Acceptance Criteria: | Given that the restaurant's computer has successfully connected to the network, then ensure that the Login Success message is displayed, then ensure switch back to the application window. |
| Definition of Done   | Unit Test Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Non-Funtional Requirement Met <br/> Product Owner Accepts User Story|
| Owner                | Vo Thien Quoc |
| Iteration:           | Unshedeled|
| Estimate:            ||
| UI |![ANH](https://gitlab.com/vothienquoc2912/21dthd4-2180601266/-/raw/main/RestaurantLogin.png?ref_type=heads)|


# 21DTHD4-2180606533
## Vo Gia Duy
## Lop: 21DTHD4
## Khoa : Conng Nghe Thong Tin



| Title | The manager checks the restaurant's turnover |
| --- | --- |
| Value Statement | As a manager, I want to checks the restaurant's turnover|
| Acceptance Criteria | <ins>Acceptance Criteria :</ins><br/> Restaurant revenue management <br/> Manage restaurant work </br> income and expenditure report for the month |
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Vo Gia Duy |

## Công nghệ thông tin

# 21dthd4 - 2180607939
## Trần Minh Quân
## Lớp: 21DTHD4
## MSSV: 2180607939
## Khoa: CNTT


| Title | Staff gets order |
| --- | --- |
| Value Statement | As a staff, I want to get customer's order |
| Acceptance Criteria | <ins>Acceptance Criteria:</ins><br/>Customers choose their food and drink.<br/>The staff check those food or drink are still available or not.<br/> If not the staff will notice customers for them to have some other options.|
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Tran Minh Quan |



# 21dthd4_2180607403
## Họ tên: Lê Anh Đại 
## MSSV: 2180607403 
## Lớp: 21DTHD4



| Title | Staff calculates the bill |
| --- | --- |
| Value Statement | As a staff, I want to calculate customer's bills |
| Acceptance Criteria | <ins>Acceptance Criteria :</ins><br/> Make a full invoice accurately for the items the customer has ordered <br/> Check the invoice and the items the customer has ordered </br> Total amount and show the bill to customer |
| Owner | Le Anh Dai |
